/* eslint-disable global-require, import/no-dynamic-require */


const processType = process.env.AUTH_PROCESS_TYPE;

let config;
try {
	config = require(`./${processType}`);
} catch (ex) {
	if (ex.code === 'MODULE_NOT_FOUND') {
		throw new Error(`No config for process type: ${processType}`);
	}

	throw ex;
}

module.exports = config;
